<!DOCTYPE html>
<html lang="en">
<head>
  <title>BMI-Rechner</title>
  <meta charset="utf-8">
  <link rel="stylesheet" href="style.css">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.1/dist/css/bootstrap.min.css" rel="stylesheet">
</head>
<body style="background-image: url(w.jpg);">
  <div class="container">
    <form method="post" action="">
        <p>Geben Sie Ihr Gewicht ein</p>
        <input type="number" name="weight">
        <p>Geben Sie Ihre Größe ein</p>
        <input type="number" name="height">
        <br>
        <button type="submit">Berechnen</button>
    </form>
  </div>

</body>
</html>